<h1> Deploy flask on Apache2 </h1>

This tutorial will help you how to deploy flask to apache server.
  
## Prerequisite

* LAMP must already on the server. If not, search for [how to install LAMP on Ubuntu server](http://howtoubuntu.org/how-to-install-lamp-on-ubuntu) then do it.

## 1. Connect to server instance

## 2 .Upload code to server ###

Usually, we will save project code folder to `/var/www/` but for permision reason we can't do it directly. So we create a folder `uploads` at user's home directory, then move to `/var/www/`

#### 2.1. Create folder `uploads` at user folder `/home/ubuntu/`

```sh
cd ~
mkdir uploads
```

#### 2.2. Upload code from client to folder server

#### 2.3. Move project folder to `/var/www/`

Back to the terminal windows that's connecting to the server.

## 3. Install python virtualenv

We use virtualenv because we dont want to keep this project is alone with other.

#### 3.1. Install virtualenv for python3
```sh
sudo apt update
pip install virtualenv
```

#### 3.2. Create virtual environment in the project folder:

```sh
cd /var/www/{name_project}/
virtualenv -p python3 venv
```

#### 3.3. Installing requirement packages:

```sh
source venv/bin/activate
pip install -r requirements.txt
```

You can check if it work correctly by execute command:

```sh
python application.py
```

If successful you will see:

```sh
 * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
```

Presss `Ctrl+C` to exit then deactivate virtualenv by execute:

```sh
deactivate
```

## 4. Create file config

Create new file using `nano`:

```sh
sudo nano /etc/apache2/sites-available/uet_interview.conf
```

And paste this code:

```sh
<VirtualHost *:80>

  ServerAdmin youremail@example.com
  ServerName sub.domain.com
  ServerAlias www.sub.domain.com

  WSGIScriptAlias / /var/www/uet_interview/uet_interview.wsgi
  <Directory /var/www/uet_interview/app/>
    Order deny,allow
    Allow from all
  </Directory>

  Alias /static /var/www/uet_interview/app/static
  <Directory /var/www/uet_interview/app/static/>
    Order allow,deny
    Allow from all
  </Directory>

  ErrorLog ${APACHE_LOG_DIR}/error.log
  LogLevel warn
  CustomLog ${APACHE_LOG_DIR}/access.log combined
  
</VirtualHost>
```

Press `Ctrl+X` then press `Y` and hit `Enter` to save the config file.

**Note**: `/var/www/uet_interview/uet_interview.wsgi` has already created in `uet_interview` project. But in new project you may need to create by yourself, and here is the file:

```python
#!/usr/bin/python3

import sys
sys.path.insert(0, '/var/www/uet_interview/')

activate_this = '/var/www/uet_interview/venv/bin/activate_this.py'
with open(activate_this) as file_:
    exec(file_.read(), dict(__file__=activate_this))

from application import application

```

## 5. Enable site

#### 5.1. Install `mod_wsgi` then enable it:

```sh
sudo apt-get install libapache2-mod-wsgi-py3
sudo a2enmod wsgi
```

#### 5.2 Enable site:

```sh
sudo a2ensite uet_interview
sudo service apache2 reload
```

That's it. Open `sub.domain.com` to check if it work.

## Debuging

If `Internal Server Error` orrcured, you may need to check the log to see what's happened:
```sh
nano /var/log/apache2/error.log
```


