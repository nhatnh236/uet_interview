# docker run --name sandbox_v4 -it -v /home/nhatnh236/uet_interview:/usercode sandbox_v4
import subprocess
import os
import uuid
import shutil

from app.classes import Output, Status

def run(source, source_extension, compile_commands, run_commands):
    result = []
    current_directory = os.getcwd()
    temp_dir = uuid.uuid4().hex
    os.makedirs('3f273357630743e4a6be91f29b37397c')
    os.chdir('3f273357630743e4a6be91f29b37397c')

    try:
        source_file_name = create_source_file(source, source_extension)
        out_compile = compile_source(compile_commands, source_file_name, result)
        execute_tests(run_commands, out_compile, result)
    except Exception as e:
        out_exception = Output()
        result.append(out_exception)
        out_exception.status = Status.ENV_CRASH
        out_exception.stderr = str(e)
        print(os.sys.exc_info())

    os.chdir(current_directory)
    shutil.rmtree('3f273357630743e4a6be91f29b37397c', True)

    return result


def execute_tests(run_command, out_compile, result):
    if out_compile.status != Status.COMPILE_ERROR:
        out_test = Output()

        if run_command:
            completed = subprocess.run(run_command,
                                        stdout=subprocess.PIPE,
                                        stderr=subprocess.PIPE,
                                        timeout=30)
            out_test.stdout = completed.stdout.decode('utf-8').rstrip()
            out_test.stderr = completed.stderr.decode('utf-8').rstrip()

            if completed.returncode:
                out_test.status = Status.RUNTIME_ERROR
            else:
                out_test.status = Status.OK

            result.append(out_test)


def compile_source(compile_commands, source_file_name, result):
    out_compile = Output()
    if compile_commands:
        compile_commands.append(source_file_name)
        completed = subprocess.run(compile_commands,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        if completed.returncode:
            result.append(out_compile)
            out_compile.status = Status.COMPILE_ERROR
            out_compile.stdout = completed.stdout.decode()
            out_compile.stderr = completed.stderr.decode()
    else:
        out_compile.status = Status.OK

    return out_compile


def create_source_file(source, source_extension):
    source_file_name = "Source." + source_extension
    text_file = open(source_file_name, "w")
    text_file.write(source)
    text_file.close()
    return source_file_name


