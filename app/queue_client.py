import zmq
import sys
import random

def run_code(lang, code):
    port = "5559"
    context = zmq.Context()
    print("Connecting to server...")
    socket = context.socket(zmq.REQ)
    socket.connect ("tcp://localhost:%s" % port)
    # send message
    socket.send_json({
        'lang' : lang,
        'code' : code
    })
    #  Get the reply.
    message = socket.recv_json()
    return message