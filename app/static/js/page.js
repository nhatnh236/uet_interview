/**
 *Object quản lý các tương tác liên quan đến modal nhập tên
 */
var objEnterName = {
	showModal: function () {
		$("body").on("click", ".usersonline ", function () {
			$(".modal-backdrop").show();
			$(".bootbox").show();
			$(".bootbox-input-text").focus();
		});
	},
	closeModal: function () {
		$("body").on("click", ".bootbox-close-button", function () {
			$(".modal-backdrop").hide();
			$(".bootbox").hide();
			let user_name = $("#user_name").val();
			if (user_name) {
				localStorage.setItem('user_name', user_name);
				$(".identicon-name").html(user_name);
			}
			else {
				localStorage.setItem('user_name', makeid(6));
			}
		});

	},
	enterName: function () {

	}


}

/**
 *Object quản lý các tương tác liên quan đến modal invite
 */
var objInvite = {
	showHideModal: function () {
		$("body").on("click", "#button_sharepad", function () {
			if ($(".sharepopover").is(':hidden') && isCalling === false) {
				$(".sharepopover").show();
			} else {
				$(".sharepopover").hide();
			}
		});

		$(document).mouseup(function (e) {
			var container = $(".sharepopover");
			var action = $('#button_sharepad span');
			// if the target of the click isn't the container nor a descendant of the container
			if (!container.is(e.target) && container.has(e.target).length === 0 && !action.is(e.target)) {
				container.hide();
			}
		});
	},
}


/**
 *Object quản lý các tương tác liên quan đến modal setting
 */
var objSetting = {
	showHideModal: function () {
		$("body").on("click", "#button_autocomplete, #button_zoomin, #button_zoomout", function () {
			if ($("#keymap").is(':hidden')) {
				$("#keymap").show();
			} else {
				$("#keymap").hide();
			}
		});

		$(document).mouseup(function (e) {
			var container = $("#keymap");
			var action = $('#button_autocomplete .glyphicon-cog');
			// if the target of the click isn't the container nor a descendant of the container
			if (!container.is(e.target) && container.has(e.target).length === 0 && !action.is(e.target)) {
				container.hide();
			}
		});
	},


}

var objLanguage = {
	showHideModal: function () {
		$("body").on("click", ".select-up", function () {
			if ($(".languageDropdown").is(':hidden')) {
				$(".languageDropdown").show();
			} else {
				$(".languageDropdown").hide();
			}
		});

		$(document).mouseup(function (e) {
			var container = $(".languageDropdown");
			var action = $('.select-up .dropup');
			// if the target of the click isn't the container nor a descendant of the container
			if (!container.is(e.target) && container.has(e.target).length === 0 && !action.is(e.target)) {
				container.hide();
			}
			$(".languageDropdown").hide();
		});
	},

	// changeLaguage: function () {
	// 	$("body").on("click", ".pack-item", function () {
	// 		var newLanguage = $(this).find("b").text();
	// 		$(".selectedLanguage").text(newLanguage);
	// 		$('.select-up').click();
	// 	});
	// },
}


var objVideo = {
	HideModal: function () {
		$("body").on("click", ".close-modal-video", function () {
			$(".modal-video").hide();
			isShowVideo = false;
		});



	},
	ShowModal: function () {
		$("body").on("click", ".show-call", function () {
			if(isShowVideo === false){
				$(".modal-video").show();
			}
		});
	}


}


$(document).ready(function () {
	objEnterName.showModal();
	objEnterName.closeModal();
	objEnterName.enterName();

	objInvite.showHideModal();

	objSetting.showHideModal();

	objLanguage.showHideModal();
	objVideo.HideModal();
	objVideo.ShowModal();
	// objLanguage.changeLaguage();

	$(function () {
		$(".resizable1").resizable(
			{
				autoHide: true,
				handles: 'e',
				resize: function (e, ui) {
					var parent = ui.element.parent();
					var remainingSpace = parent.width() - ui.element.outerWidth(),
						divTwo = ui.element.next(),
						divTwoWidth = (remainingSpace - (divTwo.outerWidth() - divTwo.width())) / parent.width() * 100 + "%";
					divTwo.width(divTwoWidth);
				},
				stop: function (e, ui) {
					var parent = ui.element.parent();
					ui.element.css(
						{
							width: ui.element.width() / parent.width() * 100 + "%",
						});
				}
			});
	});
});

function makeid(length) {
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for (var i = 0; i < length; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
}

