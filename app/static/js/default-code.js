var code_c = 
`#include <stdio.h>
int main()
{
   // printf() displays the string inside quotation
   printf("Hello, World!");
   return 0;
}`

var code_cpp =
`#include <iostream>
using namespace std;

int main() 
{
    cout << "Hello, World!";
    return 0;
}`

var code_java =
`class Source{ 
    // code in here 
    public static void main(String args[]){  
     System.out.println("Hello World");  
      }  
  } `

var code_javascript =
`console.log("Hello World")`

var code_python =
`print("Hello World")`

var code_php =
`<?php
echo "Hello World";
?>`