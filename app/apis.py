import time
from app import socketio, app, mail
from flask import render_template
from flask_mail import Message
from flask_socketio import join_room, leave_room
from .runner import run
from .classes import Status
from app.queue_client import run_code

@app.route('/')
def home():
    return render_template("recv.html")

@app.route('/interview/<peer_id>')
def viewer(peer_id):
    return render_template("caller.html", peer_id=peer_id)

@app.route('/mail')
def send_mail1():
    msg = Message('Hello', sender = 'nhathanam96@gmail.com', recipients = ['nhatnh236@gmail.com'])
    msg.body = "This is the email body"
    mail.send(msg)
    return "lol"
    
@socketio.on("EDIT_CODE")
def edit_code(msg):
    print(msg)
    room_id = msg['room']
    code = msg['code']
    peer_id = msg['peer_id']
    lang = msg['lang']
    socketio.emit('EDIT_CODE', {'code': code, 'peer_id': peer_id, 'lang':lang}, room=room_id)

@socketio.on("CHANGE_LANG")
def change_lang(msg):
    peer_id = msg['peer_id']
    lang = msg['lang']
    room_id = msg['room']
    socketio.emit("CHANGE_LANG", {'lang': lang, 'peer_id': peer_id}, room = room_id)

@socketio.on("JOIN_ZOOM")
def edit_code(msg):
    print("join: ", msg)
    room_id = msg['room']
    peer_id = msg['peer_id']
    join_room(room_id)
    socketio.emit('JOIN_NOTIFICATION', {'peer_id': peer_id}, room=room_id)

@socketio.on("LEAVE_ZOOM")
def leaveRoom(msg):
    room_id = msg['room']
    peer_id = msg['peer_id']
    leave_room(room_id)
    socketio.emit('LEAVE_ZOOM', {'peer_id': peer_id}, room=room_id)

@socketio.on("RUN_CODE")
def runCode(msg):
    room_id = msg['room']
    code = msg['code']
    lang = msg['lang']
    peer_id = msg['peer_id']
    socketio.emit('RUN_CODE', {'code': code, 'peer_id': peer_id}, room=room_id)
    time.sleep(1)
    out = run_code(lang, code)
    socketio.emit("RESULT", {"peer_id":peer_id, "result": out.get('result'), "status": out.get('status')}, room=room_id)