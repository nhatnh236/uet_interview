from flask import Flask
from flask_socketio import SocketIO
from flask_mail import Mail

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
app.config['MAIL_SERVER']='smtp.gmail.com'
app.config['MAIL_PORT'] = 587
app.config['MAIL_USERNAME'] = 'doe.optimization@gmail.com'
app.config['MAIL_PASSWORD'] = 'E2{yP7}&7c~{4e&y'
app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USE_SSL'] = False
mail = Mail(app)
socketio = SocketIO(app)
from app import apis